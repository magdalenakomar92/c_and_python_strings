#include <iostream>

using namespace std;

int dzielniki( int a, int b )
{
    if( a == 1 || a == 0 ) {
        return a;
    }
    else if( a % b == 0 )
    {
        cout << b << " ";
        return dzielniki( a / b, b );
    } else {
        return dzielniki( a / b, b + 1 );
    }
    return 0;
}

int main()
{
    int a = 0;
    cout << "Podaj dowolną liczbę: ";
    cin >> a;
    cout << "Rozkład tej liczby na czynniki pierwsze to  ";
    dzielniki( a, 2 );
}

